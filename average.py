#!/usr/bin/env python

import argparse

import porridge.cgats as cgats
import porridge.dataset as dataset

def wanted_columns(x):
    if x in [cgats.L, cgats.A, cgats.B]:
        return True
    elif x.lower().startswith(cgats.NM):
        return True
    else:
        return False

parser = argparse.ArgumentParser(description='Average some measurements.')
parser.add_argument('paths', nargs='+', help='paths to measurements')
parser.add_argument('--output', dest='filename', help='output file', required=True)
args = parser.parse_args()

ds = dataset.from_filenames(args.paths)
#grouped = ds.data.groupby(cgats.SAMPLEID)

# TODO: RGB device values?

wanted = filter(wanted_columns, ds.data.columns)

grouped = ds.data.groupby([cgats.CMYK_C, cgats.CMYK_M, cgats.CMYK_Y, cgats.CMYK_K])
mean = grouped[wanted].mean()

float_format = '%03.6f'

if args.filename:
    mean.to_csv(args.filename, sep='\t', float_format=float_format)
