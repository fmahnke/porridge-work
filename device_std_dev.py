def device_std_dev_xls(device_list, dataset, xls_path, static_fields):
    ''' Calculate std dev of measurements from several devices. '''
    xls_file = xls.get(xls_path)

    for device in device_list:
        std_dev = ds_ops.std_dev_by_sampleid(dataset.group(device))
        std_dev = static_fields.merge(std_dev, on='sampleid')
        xls.addSheet(xls_file, std_dev, device)

'''
log.info('Writing standard deviation report to ' + std_dev_filename)
static_fields = devices_dataset.data[devices_dataset.data['set_number'] == 0][['sampleid'] +
    cmyk_fields]
device_std_dev_xls(device_list, devices_dataset, std_dev_filename, static_fields)
'''
