import os
import sys

import porridge.dataset as ds
import config.paths as paths
import maxwell_fetch
import util.fileutil as futil

config_path = sys.argv[1]

config = futil.from_file(config_path)
devices = config['devices']

if not os.path.isdir(paths.MEAS_BASE_DIR):
    os.makedirs(paths.MEAS_BASE_DIR)

track_specifier = config['trackSpecifier']
mx_host = config['mxHost']
start_date = config['startDate']
end_date = config['endDate']

reference = ds.from_filenames([paths.MEAS_BASE_DIR + '/' + 'reference/iGen Iggesund Matte 300gsm 2013-11-18.txt'])

devices_with_measurements, devices_ds = maxwell_fetch.process(mx_host, track_specifier, devices, start_date, end_date)

dump_filename = track_specifier + '_dump.xlsx'

