#!/usr/bin/env python

import logging
import math
import os
import sys

import pandas as pd

import porridge.calc as calc
import porridge.cgats as cgats
import porridge.dataset as ds
import porridge.dataset_ops as ds_ops
import maxwell_fetch
import writer.xls as xls
import util.fileutil as futil

__MEASUREMENT_BASE_PATH = 'measurements'
__OUTPUT_PATH = 'output'

def _logger():
    log = logging.getLogger('maxwell')
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(levelname)s - %(funcName)s - %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)

    return log

def _get_dataset(device_list, reference_path, path):
    assert device_list is not None
    assert reference_path is not None
    assert path is not None

    ds_result = None

    for i in range(len(device_list)):
        meas_path = path + os.sep + device_list[i] + os.sep + reference_path
        meas_pattern = '*'

        if os.path.isdir(meas_path):
            if ds_result is None:
                ds_result = ds.from_file_pattern(meas_path + os.sep + meas_pattern, device_list[i])
            else:
                ds.add_file_pattern(ds_result, meas_path + os.sep + meas_pattern, device_list[i])

    return ds_result

def device_median_xls(devices_dataset, xls_path):
    ''' Calculate std dev of measurements from several devices. '''
    xls_file = xls.get(xls_path)

    med = devices_dataset.data.groupby([cgats.SAMPLEID, 'group'], as_index=False).median()
    med_renamed = pd.DataFrame(
        {cgats.SAMPLEID: med[cgats.SAMPLEID],
         'group': med['group'],
         'median_lab_b': med['lab_b'],
         'median_lab_a': med['lab_a'],
         'median_lab_l': med['lab_l'],
         })
    med_sorted = med_renamed.sort(columns=['group', cgats.SAMPLEID])

    mean_of_medians = med_sorted.groupby(cgats.SAMPLEID).mean()

    device_names = med_sorted['group'].unique()

    stats = []

    for name in device_names:
        device_measurements = med_sorted[med_sorted['group'] == name]
        device_measurements.index = device_measurements[cgats.SAMPLEID]
        diff = device_measurements - mean_of_medians
        dlab = pd.DataFrame(
            {'dl': diff['median_lab_l'],
            'da': diff['median_lab_a'],
            'db': diff['median_lab_b'],
            })
        dlab['dE76'] = dlab.apply(lambda x: math.sqrt(math.pow(x['dl'], 2) + pow(x['da'], 2) +
            pow(x['db'], 2)), axis=1)

        device_all = device_measurements.merge(dlab, left_index=True, right_index=True)

        stats.append({'device': name, 'mean dE1976': dlab['dE76'].mean(),
            'max dE1976': dlab['dE76'].max()})

        xls.addSheet(xls_file, device_all, name)

    xls.addSheet(xls_file, mean_of_medians, 'mean of devices')

    stats_df = pd.DataFrame(stats)

    xls.addSheet(xls_file, stats_df, 'summary')

    xls.addSheet(xls_file, stats_df.quantile([0.10, 0.20, 0.30, 0.40, 0.50, 0.75, 0.90, 1.0]),
        'percentile')

'''
Execution starts here.
'''

log = _logger()

if len(sys.argv) > 1:
    config_path = sys.argv[1]
else:
    print 'USAGE: ./' + sys.argv[0] + ' ' + '[config_file]'
    sys.exit(1)

config = futil.from_file(config_path)
devices = config['devices']

if not os.path.isdir(__MEASUREMENT_BASE_PATH):
    os.makedirs(__MEASUREMENT_BASE_PATH)
if not os.path.isdir(__OUTPUT_PATH):
    os.makedirs(__OUTPUT_PATH)

track_specifier = config['trackSpecifier']
mx_host = config['mxHost']
start_date = config['startDate']
end_date = config['endDate']

device_list = maxwell_fetch.process(mx_host, track_specifier, devices,
    start_date, end_date, __MEASUREMENT_BASE_PATH)

devices_dataset = _get_dataset(device_list, track_specifier, __MEASUREMENT_BASE_PATH)

log.info('Using device list ' + str(devices))
log.info('Using measurement directory ' + __MEASUREMENT_BASE_PATH);

de_analysis_filename = __OUTPUT_PATH + os.sep + track_specifier + '_de_analysis.xlsx'

log.info('Writing dE analysis report to ' + de_analysis_filename)
device_median_xls(devices_dataset, de_analysis_filename)
